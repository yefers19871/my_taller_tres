package com.castellanos.my_taller_tres;


import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;


import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

public class Mapa extends AppCompatActivity {

    MapView mapa;
    IMapController mapaControlador;
    MyLocationNewOverlay miUbicacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        mapa= findViewById(R.id.mapa);



        //obtener el contexto en el ambiente donde se encuentre la vista para configurar

        Context ctx = getApplicationContext();

        //configuración
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        // Title: teselas, baldosas, 64*64 optimizar la carga del mapase puede usar la MAPNIK en este momento

        mapa.setTileSource(TileSourceFactory.MAPNIK);
        mapa.setMultiTouchControls(true);

        String[] permisos= new String []{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
        };



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permisos,1);
        }

        //COLOCAR UN MARCADOR EN EL SITIO DEL GPS

        mapaControlador=mapa.getController();
        miUbicacion= new MyLocationNewOverlay(new GpsMyLocationProvider(this), mapa);

        // habilitar el

        miUbicacion.enableMyLocation();
        mapa.getOverlays().add(miUbicacion);

        //definir por funciones landa
        miUbicacion.runOnFirstFix(
                () -> {

                    //ejecute en el hilo de la interface grafica
                    runOnUiThread(
                            ()->{
                                mapaControlador.setZoom((double)10);
                                mapaControlador.setCenter(miUbicacion.getMyLocation());

                                String latitud= String.valueOf(miUbicacion.getMyLocation().getLatitude());
                                String longitud= String.valueOf(miUbicacion.getMyLocation().getLongitude());
                                Log.d(TAG, "Ubicado!!!"+latitud+longitud);


                                //Log.d(TAG,"Ubicado!!!!!! yefer" + miUbicacion.getMyLocation().getLatitude()+miUbicacion.getMyLocation().getLongitude());
                            }
                    );

                }
        );

        /*

        // colocar un marcador en el mapa

        GeoPoint miPunto= new GeoPoint(7.123244770085014, -73.13737051687372);
        Marker miMarcador= new Marker(mapa);
        miMarcador.setPosition(miPunto);
        mapa.getOverlays().add(miMarcador);

        GeoPoint puntoUno= new GeoPoint(7.123244770085019, -73.13737051687375);
        Marker marcadorUno= new Marker(mapa);
        marcadorUno.setPosition(puntoUno);
        mapa.getOverlays().add(marcadorUno);

        // se puede hacer un arreglo y agregar los marcadores al mapa para no agregar uno por uno como las lineas anteriores


        //taller: enviar al chat de whatssap el mapa centrado en esa posición

        // debemos crear un controlador del mapa

        mapaControlador=mapa.getController();
        mapaControlador.setZoom(20.0);
        mapaControlador.setCenter(miPunto);
*/
    }
}