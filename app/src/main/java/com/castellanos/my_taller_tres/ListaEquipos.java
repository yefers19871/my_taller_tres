package com.castellanos.my_taller_tres;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ListaEquipos extends AppCompatActivity {

    WebView paginaEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_equipos);
        paginaEquipo=findViewById(R.id.paginaEquipo);

        //Capturar el objeto Intent
        Intent myIntent=getIntent();

        String dato=myIntent.getStringExtra("nombre");

        dato.replaceAll(" ", "_");

        String url="https://es.wikipedia.org/wiki/"+dato;

        //Deshabilitar el navegador predeterminado
        paginaEquipo.setWebViewClient(new WebViewClient());

        paginaEquipo.loadUrl(url);


    }
}
