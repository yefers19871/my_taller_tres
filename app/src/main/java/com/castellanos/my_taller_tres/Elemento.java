package com.castellanos.my_taller_tres;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


//tercer paso del recyclerView, esta clase es para el viewholder siendo el cajon

public class Elemento extends RecyclerView.ViewHolder {

    //si en el layout tiene un elemento de tipo textView, debe crearlo aca

    TextView tvElemento;

    //con este constructor se le pasa el layout que se va a utilizar
    public Elemento(@NonNull View itemView) {
        super(itemView);

        // crear un objeto de la clase Elemento para guardar los datos
        tvElemento= itemView.findViewById(R.id.tvElemento);
    }
}
