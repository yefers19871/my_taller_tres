package com.castellanos.my_taller_tres;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.opengl.ETC1;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {

    //crear un objeto recyclerview
    RecyclerView rvElementos;

    //crear el objeto
    Adaptador miAdaptador;

    Button Calculadora,Mapa,EquiposFut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //tener el recyclerview
        rvElementos=findViewById(R.id.tvElemento);

        rvElementos.setLayoutManager(new LinearLayoutManager(this));

        //definir datos pueden obtenerse de diferentes sitios

        //1. datos locales definidos dentro de la app aquí:

        List<String> misDatosLocales= new ArrayList<>();
        misDatosLocales.add("Yefer Alexander");
        misDatosLocales.add("Castellanos Parra");
        misDatosLocales.add("Policía Nacional");


        //tomar los datos desde un array

        List<String> misDatosString= Arrays.asList(getResources().getStringArray(R.array.nombres));



        // crear un robot de la clase adpatador para que tome los datos y los meta en una cajita y los ubique en un recycler view

        miAdaptador= new Adaptador(misDatosLocales);
        rvElementos.setAdapter(miAdaptador);


        Calculadora= (Button)findViewById(R.id.Calculadora);

        Calculadora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Calculadora = new Intent(MainActivity2.this, Calculadora.class);
                startActivity(Calculadora);
            }
        }

        );

        Mapa= (Button)findViewById(R.id.Mapa);

        Mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Mapa = new Intent(MainActivity2.this, Mapa.class);
                startActivity(Mapa);
            }
        }

        );

        EquiposFut= (Button)findViewById(R.id.EquiposFutbol);

        EquiposFut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent EquiposFut = new Intent(MainActivity2.this, MainActivityEquipo.class);
                startActivity(EquiposFut);
            }
        }

        );


    }

}