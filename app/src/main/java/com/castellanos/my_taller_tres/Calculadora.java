package com.castellanos.my_taller_tres;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.mariuszgromada.math.mxparser.Expression;

public class Calculadora extends AppCompatActivity implements View.OnClickListener{


    EditText expresion;

    Button bevaluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        expresion=findViewById(R.id.expresion);
        bevaluar=findViewById(R.id.evaluar);
        bevaluar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        String valor= expresion.getText().toString();
        Expression analizador=new Expression(valor);
        Double resultado= analizador.calculate();

        if(Double.isNaN(resultado)){

            expresion.setText("Por favor aprenda a manejar calculadoras!!!");

        }else {
            expresion.setText(Double.toString(resultado));
        }

    }
}